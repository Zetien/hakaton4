import { useEffect, useState } from 'react';
import { registerEspecialidad } from '../Especialidades/EspecialidadesServer';
import { useNavigate, useParams } from "react-router-dom";
import * as EspecialidadesServer from '../Especialidades/EspecialidadesServer';


const EspecialidadesForm =()=>{
    const navigate = useNavigate();
    const params = useParams();

    

    const initialState = {nombre:'',descripcion:'',usuario_registro:'',usuario_modificacion:'',activo:'true'};
    
    const [especialidad, setEspecialidad] = useState(initialState);

    const handleInputChange = (e) => {
        setEspecialidad({
            ...especialidad,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerEspecialidad(especialidad).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await EspecialidadesServer.updateEspecialidad(params.id,especialidad); 
            }
            
            navigate("/especialidadesList");
        }catch(error){
            console.log(error);
        }
    };

    const getEspecialidad=async(especialidadId) => {
        try{
          const rest=await EspecialidadesServer.getEspecialidad(especialidadId);
          const data=await rest.json();
          const {nombre,descripcion,usuario_registro,usuario_modificacion,activo}=data;
          setEspecialidad({nombre,descripcion,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
      };  

    useEffect(() => {
        if(params.id){
            getEspecialidad(params.id);
        }
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Agregar Especialidades</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="nombreEspecialidad" className="form-label">Nombre Especialidad</label>
                    <input type="text" className="form-control" name="nombre" value={especialidad.nombre} onChange={handleInputChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="descripcion" className="form-label">Descripcion</label>
                    <input type="text" className="form-control" name="descripcion" value={especialidad.descripcion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={especialidad.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={especialidad.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={especialidad.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default EspecialidadesForm;