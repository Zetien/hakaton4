import React from "react"
import { deleteMedico } from '../Medicos/MedicosServer';
import { useNavigate } from "react-router-dom";


const MedicoItem =({medico})=>{
    const navigate = useNavigate();

    const handleDelete=(medicoId)=>{
        deleteMedico(medicoId)
    };

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">{medico.nombres}</div>
                <div className="card-body">
                    <h5 className="card-title">{medico.apellidos}</h5>
                    <p className="card-text">Dni:&nbsp;{medico.dni}</p>
                    <p className="card-text">Direccion:&nbsp;{medico.direccion}</p>
                    <p className="card-text">Correo:&nbsp;{medico.correo}</p>
                    <p className="card-text">Telefono:&nbsp;{medico.telefono}</p>
                    <p className="card-text">Sexo:&nbsp;{medico.sexo}</p>
                    <p className="card-text">Num Colegiatura:&nbsp;{medico.num_colegiatura}</p>
                    <p className="card-text">Fecha Nacimiento:&nbsp;{medico.fecha_nacimiento}</p>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>medico.id && handleDelete(medico.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updateMedico/${medico.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default MedicoItem;