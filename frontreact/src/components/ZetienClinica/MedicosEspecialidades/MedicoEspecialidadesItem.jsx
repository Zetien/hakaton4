import React from "react"
import { deleteMedicoEspecialidade } from '../MedicosEspecialidades/MedicoEspecialidadesServer';
import { useNavigate } from "react-router-dom";
import * as MedicosServer from '../Medicos/MedicosServer'
import * as EspecialidadesServer from '../Especialidades/EspecialidadesServer';
import { useEffect, useState } from "react";

const MedicoEspecialidadeItem =({medicoespecialidade})=>{
    const navigate = useNavigate();
    const [medicos, setMedicos] = useState([]);
    const [especialidades, setEspecialidades] = useState([]);

    const handleDelete=(medicoespecialidadeId)=>{
        deleteMedicoEspecialidade(medicoespecialidadeId)
    
    };

    const buscarMedico=(medicoId)=>{
        return medicos.find((medico)=>
            medico.id===medicoId
        )?.nombres + " " + medicos.find((medico)=>
            medico.id===medicoId)?.apellidos;
    }

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };

    const buscarEspecialidad=(especialidadId)=>{
        return especialidades.find((especialidad)=>
            especialidad.id===especialidadId
        )?.nombre + " " + especialidades.find((especialidad)=>
            especialidad.id===especialidadId)?.descripcion;
    }

    const listEspecialidades = async() => {
        try{
            const res= await EspecialidadesServer.listEspecialidades();
            const data = await res.json();
            setEspecialidades(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
        listEspecialidades();
    },[]);

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">{buscarMedico(medicoespecialidade.medico_id)}</div>
                <div className="card-body">
                <div className="card-header">{buscarEspecialidad(medicoespecialidade.especialidad_id)}</div>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>medicoespecialidade.id && handleDelete(medicoespecialidade.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updateMedicoEspecialidade/${medicoespecialidade.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default MedicoEspecialidadeItem;