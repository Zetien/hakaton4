import React from "react"
import { deletePaciente } from '../Pacientes/PacientesServer';
import { useNavigate } from "react-router-dom";


const PacienteItem =({paciente})=>{
    const navigate = useNavigate();

    const handleDelete=(pacienteId)=>{
        deletePaciente(pacienteId)
    };

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">{paciente.nombres}</div>
                <div className="card-body">
                    <h5 className="card-title">{paciente.apellidos}</h5>
                    <p className="card-text">Dni:&nbsp;{paciente.dni}</p>
                    <p className="card-text">Direccion:&nbsp;{paciente.direccion}</p>
                    <p className="card-text">Telefono:&nbsp;{paciente.telefono}</p>
                    <p className="card-text">Sexo:&nbsp;{paciente.sexo}</p>
                    <p className="card-text">Fecha Nacimiento:&nbsp;{paciente.fecha_nacimiento}</p>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>paciente.id && handleDelete(paciente.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updatePaciente/${paciente.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default PacienteItem;