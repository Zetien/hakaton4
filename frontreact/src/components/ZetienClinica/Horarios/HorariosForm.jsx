import { useEffect, useState } from 'react';
import { registerHorario } from '../Horarios/HorariosServer';
import { useNavigate, useParams } from "react-router-dom";
import * as HorariosServer from '../Horarios/HorariosServer';
import * as MedicosServer from '../Medicos/MedicosServer'


const HorariosForm =()=>{
    const navigate = useNavigate();
    const params = useParams();
    const [medicos, setMedicos] = useState([]);

    

    const initialState = {
        medico_id:'',
        fecha_atencion:'',
        inicio_atencion:'',
        fin_atencion:'',
        usuario_registro:'',
        usuario_modificacion:'',
        activo:'true'
    };
    
    const [horario, setHorario] = useState(initialState);

    const handleInputChange = (e) => {
        setHorario({
            ...horario,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerHorario(horario).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await HorariosServer.updateHorario(params.id,horario); 
            }
            
            navigate("/horariosList");
        }catch(error){
            console.log(error);
        }
    };

    const getHorario=async(horarioId) => {
        try{
          const rest=await HorariosServer.getHorario(horarioId);
          const data=await rest.json();
          const {medico_id,fecha_atencion,inicio_atencion,fin_atencion,usuario_registro,usuario_modificacion,activo}=data;
          setHorario({medico_id,fecha_atencion,inicio_atencion,fin_atencion,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
    };  

    useEffect(() => {
        if(params.id){
            getHorario(params.id);
        }
    },[]);

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Agregar Horarios</h1>
            <form onSubmit={handleSubmit}>
                
                <div className="form-group">
                    <label htmlFor="">Medico</label>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        name="medico_id"
                        value={horario.medico_id}
                        onChange={handleInputChange}
                    >
                    <option defaultValue="None">Selecciona un medico</option>
                    {medicos.map((medico) => {
                    return (
                    <option key={medico.id} value={medico.id} name="medico_id">
                    {medico.nombres}
                    </option>
                    );
                    })}
                    </select>
                </div> 

                <div className="mb-3">
                    <label htmlFor="fechaAtencion" className="form-label">Fecha Atencion</label>
                    <input type="date" className="form-control" name="fecha_atencion" value={horario.fecha_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="inicioAtencion" className="form-label">Inicio Atencion</label>
                    <input type="time" className="form-control" name="inicio_atencion" value={horario.inicio_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="finAtencion" className="form-label">Fin Atencion</label>
                    <input type="time" className="form-control" name="fin_atencion" value={horario.fin_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={horario.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={horario.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={horario.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default HorariosForm;