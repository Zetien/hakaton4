import { useEffect, useState } from 'react';
import { registerMedicoEspecialidade } from '../MedicosEspecialidades/MedicoEspecialidadesServer';
import { useNavigate, useParams } from "react-router-dom";
import * as MedicoEspecialidadesServer from '../MedicosEspecialidades/MedicoEspecialidadesServer';
import * as MedicosServer from '../Medicos/MedicosServer'
import * as EspecialidadesServer from '../Especialidades/EspecialidadesServer';


const MedicoEspecialidadesForm =()=>{
    const navigate = useNavigate();
    const params = useParams();
    const [medicos, setMedicos] = useState([]);
    const [especialidades, setEspecialidades] = useState([]);

    

    const initialState = {
        medico_id:'',
        especialidad_id:'',
        usuario_registro:'',
        usuario_modificacion:'',
        activo:'true'
    };
    
    const [medicoespecialidade, setMedicoEspecialidade] = useState(initialState);

    const handleInputChange = (e) => {
        setMedicoEspecialidade({
            ...medicoespecialidade,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerMedicoEspecialidade(medicoespecialidade).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await MedicoEspecialidadesServer.updateMedicoEspecialidade(params.id,medicoespecialidade); 
            }
            
            navigate("/medicoespecialidadesList");
        }catch(error){
            console.log(error);
        }
    };

    const getMedicoEspecialidade=async(medicoespecialidadeId) => {
        try{
          const rest=await MedicoEspecialidadesServer.getMedicoEspecialidade(medicoespecialidadeId);
          const data=await rest.json();
          const {medico_id,especialidad_id,usuario_registro,usuario_modificacion,activo}=data;
          setMedicoEspecialidade({medico_id,especialidad_id,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
    };  

    useEffect(() => {
        if(params.id){
            getMedicoEspecialidade(params.id);
        }
    },[]);

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };

    const listEspecialidades = async() => {
        try{
            const res= await EspecialidadesServer.listEspecialidades();
            const data = await res.json();
            setEspecialidades(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
        listEspecialidades();
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Medicos - Especialidades</h1>
            <form onSubmit={handleSubmit}>
                
                <div className="form-group">
                    <label htmlFor="">Medico</label>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        name="medico_id"
                        value={medicoespecialidade.medico_id}
                        onChange={handleInputChange}
                    >
                    <option defaultValue="None">Selecciona un medico</option>
                    {medicos.map((medico) => {
                    return (
                    <option key={medico.id} value={medico.id} name="medico_id">
                    {medico.nombres}
                    </option>
                    );
                    })}
                    </select>
                </div> 
                <div className="form-group">
                    <label htmlFor="">Especialidad</label>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        name="especialidad_id"
                        value={medicoespecialidade.especialidad_id}
                        onChange={handleInputChange}
                    >
                    <option defaultValue="None">Selecciona una especialidad</option>
                    {especialidades.map((especialidad) => {
                    return (
                    <option key={especialidad.id} value={especialidad.id} name="especialidad_id">
                    {especialidad.nombre}
                    </option>
                    );
                    })}
                    </select>
                </div> 
                             
                <div className="mb-3">
                    <label htmlFor="MedicoEspecialidadRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={medicoespecialidade.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="MedicoEspecialidadModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={medicoespecialidade.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={medicoespecialidade.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default MedicoEspecialidadesForm;