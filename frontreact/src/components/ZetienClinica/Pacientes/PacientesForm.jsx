import { useEffect, useState } from 'react';
import { registerPaciente } from '../Pacientes/PacientesServer';
import { useNavigate, useParams } from "react-router-dom";
import * as PacientesServer from '../Pacientes/PacientesServer';


const PacientesForm =()=>{
    const navigate = useNavigate();
    const params = useParams();

    

    const initialState = {
    nombres:'',
    apellidos:'',
    dni:'',
    direccion:'',
    telefono:'',
    sexo:'',
    fecha_nacimiento:'',
    usuario_registro:'',
    usuario_modificacion:'',
    activo:'true'
};
    
    const [paciente, setPaciente] = useState(initialState);

    const handleInputChange = (e) => {
        setPaciente({
            ...paciente,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerPaciente(paciente).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await PacientesServer.updatePaciente(params.id,paciente); 
            }
            
            navigate("/pacientesList");
        }catch(error){
            console.log(error);
        }
    };

    const getPaciente=async(pacienteId) => {
        try{
          const rest=await PacientesServer.getPaciente(pacienteId);
          const data=await rest.json();
          const {nombres,apellidos,dni,direccion,telefono,sexo,fecha_nacimiento,usuario_registro,usuario_modificacion,activo}=data;
          setPaciente({nombres,apellidos,dni,direccion,telefono,sexo,fecha_nacimiento,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
      };  

    useEffect(() => {
        if(params.id){
            getPaciente(params.id);
        }
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Agregar Paciente</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="nombrePaciente" className="form-label">Nombres</label>
                    <input type="text" className="form-control" name="nombres" value={paciente.nombres} onChange={handleInputChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="apellidoPaciente" className="form-label">Apellidos</label>
                    <input type="text" className="form-control" name="apellidos" value={paciente.apellidos} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="dniPaciente" className="form-label">DNI</label>
                    <input type="number" className="form-control" name="dni" value={paciente.dni} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="direccionPaciente" className="form-label">Direccion</label>
                    <input type="text" className="form-control" name="direccion" value={paciente.direccion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="telefonoPaciente" className="form-label">Telefono</label>
                    <input type="number" className="form-control" name="telefono" value={paciente.telefono} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="sexoPaciente" className="form-label">Sexo</label>
                    <input type="text" className="form-control" name="sexo" value={paciente.sexo} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="fechaNacimientoPaciente" className="form-label">Fecha Nacimiento</label>
                    <input type="date" className="form-control" name="fecha_nacimiento" value={paciente.fecha_nacimiento} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={paciente.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={paciente.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={paciente.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default PacientesForm;