import React, {useEffect, useState} from 'react';
import * as PacientesServer from '../Pacientes/PacientesServer'
import PacienteItem from '../Pacientes/PacienteItem'
import {Link} from 'react-router-dom'

const PacientesList = () => {
    const [pacientes, setPacientes] = useState([]);

    const listPacientes = async() => {
        try{
          const res= await PacientesServer.listPacientes(); 
          const data = await res.json();
          setPacientes(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listPacientes();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Pacientes</h1>
           <div className="row">
           {pacientes.map((paciente) => (
               <PacienteItem key ={paciente.id} paciente ={paciente}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/pacientesForm" role="button">Agregar Paciente</Link>
               </div>
       </div>
   );
};

export default PacientesList;