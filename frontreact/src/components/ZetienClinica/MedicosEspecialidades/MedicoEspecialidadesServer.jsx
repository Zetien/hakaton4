import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/medico_especialidade/';

export const listMedicoEspecialidades = async () => {
    return await fetch(API_URL);
};

 export const getMedicoEspecialidade=async(medicoespecialidadeId) => {
    return await fetch(`${API_URL}${medicoespecialidadeId}`);
}; 




export const registerMedicoEspecialidade = async (MedicoEspecialidade) => {
    
    return axios.post(API_URL, MedicoEspecialidade,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updateMedicoEspecialidade = async (medicoespecialidadeId,updatedMedicoEspecialidade) => {
    
    return axios.put(`${API_URL}${medicoespecialidadeId}/`,updatedMedicoEspecialidade,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deleteMedicoEspecialidade = async (medicoespecialidadeId) => {
    return axios.delete(`${API_URL}${medicoespecialidadeId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}
