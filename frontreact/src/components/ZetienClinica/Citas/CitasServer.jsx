import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/cita/';

export const listCitas = async () => {
    return await fetch(API_URL);
};

 export const getCita=async(citaId) => {
    return await fetch(`${API_URL}${citaId}`);
}; 




export const registerCita = async (Cita) => {
    
    return axios.post(API_URL, Cita,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updateCita = async (citaId,updatedCita) => {
    
    return axios.put(`${API_URL}${citaId}/`,updatedCita,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deleteCita = async (citaId) => {
    return axios.delete(`${API_URL}${citaId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}
