import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/especialidade/';

export const listEspecialidades = async () => {
    return await fetch(API_URL);
};

export const getEspecialidad=async(especialidadId) => {
    return await fetch(`${API_URL}${especialidadId}`);
}

export const registerEspecialidad = async (Especialidad) => {
    console.log(Especialidad);
    return axios.post(API_URL, Especialidad,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updateEspecialidad = async (especialidadId,updatedEspecialidad) => {
    
    return axios.put(`${API_URL}${especialidadId}/`,updatedEspecialidad,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deleteEspecialidad = async (especialidadId) => {
    return axios.delete(`${API_URL}${especialidadId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}



