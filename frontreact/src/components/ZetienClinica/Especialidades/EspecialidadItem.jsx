import React from "react"
import { deleteEspecialidad } from '../Especialidades/EspecialidadesServer';
import { useNavigate } from "react-router-dom";


const EspecialidadItem =({especialidad})=>{
    const navigate = useNavigate();

    const handleDelete=(especialidadId)=>{
        deleteEspecialidad(especialidadId)
    };

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">{especialidad.nombre}</div>
                <div className="card-body">
                    <h5 className="card-title">{especialidad.descripcion}</h5>
                    <p className="card-text">{especialidad.fecha_registro}</p>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>especialidad.id && handleDelete(especialidad.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updateEspecialidad/${especialidad.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default EspecialidadItem;

