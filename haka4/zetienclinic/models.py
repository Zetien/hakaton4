from django.db import models
from django.contrib.auth.models import User


class Medico(models.Model):
    nombres = models.CharField(max_length=30)
    apellidos = models.CharField(max_length=30)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=100)
    correo = models.EmailField(max_length=50)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=30)
    num_colegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    usuario_modificacion =  models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombres + " " + self.apellidos

class Paciente(models.Model):
    nombres = models.CharField(max_length=30)
    apellidos = models.CharField(max_length=30)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=100)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=30)
    fecha_nacimiento = models.DateField()
    fecha_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombres + " " + self.apellidos

class Especialidade(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=100)
    fecha_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre

class Medico_especialidade(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad_id = models.ForeignKey(Especialidade, on_delete=models.CASCADE)
    fecha_registro = models.DateField(auto_now=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField(default=True)

    def __str__(self):
        return str(self.especialidad_id) 

class Horario(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    activo = models.BooleanField()
    fecha_registro = models.DateField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    
    def __str__(self):
        return str(self.medico_id) + " " + str(self.fecha_atencion)

class Cita(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    estado = models.CharField(max_length=30)
    observaciones = models.CharField(max_length=100)
    activo = models.BooleanField(default=True)
    fecha_registro = models.DateField(auto_now_add=True)
    usuario_registro = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')

    def __str__(self):
        return str(self.medico_id) + " " + str(self.paciente_id) + " " + str(self.fecha_atencion)




    

    