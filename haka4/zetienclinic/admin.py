from django.contrib import admin
from zetienclinic.models import Horario, Medico, Paciente, Especialidade, Medico_especialidade, Cita

@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    list_display = ('nombres', 'apellidos', 'dni', 'direccion', 'correo', 'telefono', 'sexo', 'num_colegiatura', 'fecha_nacimiento', 'fecha_registro', 'fecha_modificacion', 'usuario_registro', 'usuario_modificacion', 'activo')

@admin.register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = ('nombres', 'apellidos', 'dni', 'direccion', 'telefono', 'sexo', 'fecha_nacimiento', 'fecha_registro', 'fecha_modificacion', 'usuario_registro', 'usuario_modificacion', 'activo')

@admin.register(Especialidade)
class EspecialidadeAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'fecha_registro', 'fecha_modificacion', 'usuario_registro', 'usuario_modificacion', 'activo')

@admin.register(Medico_especialidade)
class Medico_especialidadeAdmin(admin.ModelAdmin):
    list_display = ('medico_id', 'especialidad_id', 'fecha_registro', 'fecha_modificacion', 'usuario_registro', 'usuario_modificacion', 'activo')

@admin.register(Horario)
class HorarioAdmin(admin.ModelAdmin):
    list_display = ('medico_id', 'fecha_atencion', 'inicio_atencion', 'fin_atencion', 'activo', 'fecha_registro', 'usuario_registro', 'usuario_modificacion', 'fecha_modificacion', 'usuario_modificacion')

@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = ('medico_id', 'paciente_id','fecha_atencion', 'inicio_atencion', 'fin_atencion', 'estado', 'observaciones', 'activo', 'fecha_registro', 'usuario_registro','usuario_modificacion')