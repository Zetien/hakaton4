from django.apps import AppConfig


class ZetienclinicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zetienclinic'
