import React, {useEffect, useState} from 'react';
import * as HorariosServer from '../Horarios/HorariosServer'
import HorarioItem from '../Horarios/HorarioItem'
import {Link} from 'react-router-dom'
import axios from 'axios';

const HorariosList = () => {
    const [horarios, setHorarios] = useState([]);

    const listHorarios = async() => {
        try{
          const res= await HorariosServer.listHorarios(); 
          const data = await res.json();
          setHorarios(data.results);
        }catch(error){
            console.log(error);
        }
    };

    
   
    useEffect(() => {
        listHorarios();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Horarios</h1>
           <div className="row">
           {horarios.map((horario) => (
               <HorarioItem key ={horario.id} horario ={horario}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/horariosForm" role="button">Agregar Horario</Link>
               </div>
       </div>
   );
};

export default HorariosList;