import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import EspecialidadesList from './components/ZetienClinica/Especialidades/EspecialidadesList'
import EspecialidadesForm from './components/ZetienClinica/Especialidades/EspecialidadesForm'
import MedicosList from './components/ZetienClinica/Medicos/MedicosList'
import MedicosForm from './components/ZetienClinica/Medicos/MedicosForm'
import PacientesList from './components/ZetienClinica/Pacientes/PacientesList'
import PacientesForm from './components/ZetienClinica/Pacientes/PacientesForm'
import HorariosList from './components/ZetienClinica/Horarios/HorariosList'
import HorariosForm from './components/ZetienClinica/Horarios/HorariosForm'
import MedicoEspecialidadesList from './components/ZetienClinica/MedicosEspecialidades/MedicoEspecialidadesList'
import MedicoEspecialidadesForm from './components/ZetienClinica/MedicosEspecialidades/MedicoEspecialidadesForm'
import CitasList from './components/ZetienClinica/Citas/CitasList'
import CitasForm from './components/ZetienClinica/Citas/CitasForm'

import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from './components/Navbar/Navbar'
import Footer from './components/Footer/Footer'
import Home from './components/Pages/Home'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
    <Navbar/>
    <div className="container my-4">
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path="/especialidadesForm" element={<EspecialidadesForm/>} />
        <Route path="/especialidadesList" element={<EspecialidadesList/>} />
        <Route path="/updateEspecialidad/:id" element={<EspecialidadesForm/>} />
        <Route path="/medicosForm" element={<MedicosForm/>} />
        <Route path="/medicosList" element={<MedicosList/>} />
        <Route path="/updateMedico/:id" element={<MedicosForm/>} />
        <Route path="/pacientesForm" element={<PacientesForm/>} />
        <Route path="/pacientesList" element={<PacientesList/>} />
        <Route path="/updatePaciente/:id" element={<PacientesForm/>} />
        <Route path="/horariosForm" element={<HorariosForm/>} />
        <Route path="/horariosList" element={<HorariosList/>} />
        <Route path="/updateHorario/:id" element={<HorariosForm/>} />
        <Route path="/medicoEspecialidadesForm" element={<MedicoEspecialidadesForm/>} />
        <Route path="/medicoEspecialidadesList" element={<MedicoEspecialidadesList/>} />
        <Route path="/updateMedicoEspecialidade/:id" element={<MedicoEspecialidadesForm/>} />
        <Route path="/citasForm" element={<CitasForm/>} />
        <Route path="/citasList" element={<CitasList/>} />
        <Route path="/updateCita/:id" element={<CitasForm/>} />
      </Routes>
    </div>
    <Footer/>
    </BrowserRouter>
    </React.StrictMode>
)
