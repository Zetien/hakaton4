import React from "react"
import { deleteCita } from '../Citas/CitasServer';
import { useNavigate } from "react-router-dom";
import * as MedicosServer from '../Medicos/MedicosServer'
import * as PacientesServer from '../Pacientes/PacientesServer';
import { useEffect, useState } from "react";

const CitaItem =({cita})=>{
    const navigate = useNavigate();
    const [medicos, setMedicos] = useState([]);
    const [pacientes, setPacientes] = useState([]);

    const handleDelete=(citaId)=>{
        deleteCita(citaId)
    
    };

    const buscarMedico=(medicoId)=>{
        return medicos.find((medico)=>
            medico.id===medicoId
        )?.nombres + " " + medicos.find((medico)=>
            medico.id===medicoId)?.apellidos;
    }

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };

    const buscarPaciente=(pacienteId)=>{
        return pacientes.find((paciente)=>
            paciente.id===pacienteId
        )?.nombres + " " + pacientes.find((paciente)=>
            paciente.id===pacienteId)?.apellidos;
    }

    const listPacientes = async() => {
        try{
            const res= await PacientesServer.listPacientes();
            const data = await res.json();
            setPacientes(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
        listPacientes();
    },[]);

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">Medico:&nbsp;{buscarMedico(cita.medico_id)}</div>
                <div className="card-body">
                <div className="card-header">Paciente:&nbsp;{buscarPaciente(cita.paciente_id)}</div>
                <div className="card-header">Fecha de la cita:&nbsp;{cita.fecha_atencion}</div>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>cita.id && handleDelete(cita.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updateCita/${cita.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default CitaItem;