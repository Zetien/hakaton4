import React, {useEffect, useState} from 'react';
import * as CitasServer from '../Citas/CitasServer'
import CitasItem from '../Citas/CitasItem';
import {Link} from 'react-router-dom'
import axios from 'axios';

const CitasList = () => {
    const [cita, setCitas] = useState([]);

    const listCitas = async() => {
        try{
          const res= await CitasServer.listCitas(); 
          const data = await res.json();
          setCitas(data.results);
        }catch(error){
            console.log(error);
        }
    };

    
   
    useEffect(() => {
        listCitas();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Citas</h1>
           <div className="row">
           {cita.map((cita) => (
               <CitasItem key ={cita.id} cita ={cita}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/citasForm" role="button">Agregar Citas Medicas</Link>
               </div>
       </div>
   );
};

export default CitasList;