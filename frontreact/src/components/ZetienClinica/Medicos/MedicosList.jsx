import React, {useEffect, useState} from 'react';
import * as MedicosServer from '../Medicos/MedicosServer'
import MedicoItem from '../Medicos/MedicoItem'
import {Link} from 'react-router-dom'

const MedicosList = () => {
    const [medicos, setMedicos] = useState([]);

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Medicos</h1>
           <div className="row">
           {medicos.map((medico) => (
               <MedicoItem key ={medico.id} medico ={medico}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/medicosForm" role="button">Agregar Medico</Link>
               </div>
       </div>
   );
};

export default MedicosList;