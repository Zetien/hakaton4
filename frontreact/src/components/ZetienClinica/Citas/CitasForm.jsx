import { useEffect, useState } from 'react';
import { registerCita } from '../Citas/CitasServer';
import { useNavigate, useParams } from "react-router-dom";
import * as CitasServer from '../Citas/CitasServer';
import * as MedicosServer from '../Medicos/MedicosServer'
import * as PacientesServer from '../Pacientes/PacientesServer';


const CitasForm =()=>{
    const navigate = useNavigate();
    const params = useParams();
    const [medicos, setMedicos] = useState([]);
    const [pacientes, setPacientes] = useState([]);

    

    const initialState = {
        medico_id:'',
        paciente_id:'',
        fecha_atencion:'',
        inicio_atencion:'',
        fin_atencion:'',
        estado:'',
        observaciones:'',
        usuario_registro:'',
        usuario_modificacion:'',
        activo:'true'
    };
    
    const [cita, setCita] = useState(initialState);

    const handleInputChange = (e) => {
        setCita({
            ...cita,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerCita(cita).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await CitasServer.updateMedicoEspecialidade(params.id,cita); 
            }
            
            navigate("/citasList");
        }catch(error){
            console.log(error);
        }
    };

    const getCita=async(citaId) => {
        try{
          const rest=await CitasServer.getCita(citaId);
          const data=await rest.json();
          const {medico_id,paciente_id,fecha_atencion,inicio_atencion,fin_atencion,estado,observaciones,usuario_registro,usuario_modificacion,activo}=data;
          setCita({medico_id,paciente_id,fecha_atencion,inicio_atencion,fin_atencion,estado,observaciones,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
    };  

    useEffect(() => {
        if(params.id){
            getCita(params.id);
        }
    },[]);

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };

    const listPacientes = async() => {
        try{
            const res= await PacientesServer.listPacientes();
            const data = await res.json();
            setPacientes(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
        listPacientes();
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Citas</h1>
            <form onSubmit={handleSubmit}>
                
                <div className="form-group">
                    <label htmlFor="">Medico</label>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        name="medico_id"
                        value={cita.medico_id}
                        onChange={handleInputChange}
                    >
                    <option defaultValue="None">Selecciona un medico</option>
                    {medicos.map((medico) => {
                    return (
                    <option key={medico.id} value={medico.id} name="medico_id">
                    {medico.nombres}
                    </option>
                    );
                    })}
                    </select>
                </div> 
                <div className="form-group">
                    <label htmlFor="">Paciente</label>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        name="paciente_id"
                        value={cita.especialidad_id}
                        onChange={handleInputChange}
                    >
                    <option defaultValue="None">Selecciona un paciente</option>
                    {pacientes.map((paciente) => {
                    return (
                    <option key={paciente.id} value={paciente.id} name="paciente_id">
                    {paciente.nombres}
                    </option>
                    );
                    })}
                    </select>
                </div> 
                <div className="mb-3">
                    <label htmlFor="CitaFechaAtencion" className="form-label">Fecha Atencion</label>
                    <input type="date" className="form-control" name="fecha_atencion" value={cita.fecha_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaInicioAtencion" className="form-label">Inicio Atencion</label>
                    <input type="time" className="form-control" name="inicio_atencion" value={cita.inicio_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaFinAtencion" className="form-label">Fin Atencion</label>
                    <input type="time" className="form-control" name="fin_atencion" value={cita.fin_atencion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaEstado" className="form-label">Estado</label>
                    <input type="text" className="form-control" name="estado" value={cita.estado} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaObaservaciones" className="form-label">Observaciones</label>
                    <input type="text" className="form-control" name="observaciones" value={cita.observaciones} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={cita.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="CitaModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={cita.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={cita.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default CitasForm;