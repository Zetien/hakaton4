import React, {useEffect, useState} from 'react';
import * as MedicoEspecialidadesServer from '../MedicosEspecialidades/MedicoEspecialidadesServer'
import MedicoEspecialidadesItem from '../MedicosEspecialidades/MedicoEspecialidadesItem';
import {Link} from 'react-router-dom'
import axios from 'axios';

const MedicoEspecialidadesList = () => {
    const [medicoespecialidade, setMedicoEspecialidades] = useState([]);

    const listMedicoEspecialidades = async() => {
        try{
          const res= await MedicoEspecialidadesServer.listMedicoEspecialidades(); 
          const data = await res.json();
          setMedicoEspecialidades(data.results);
        }catch(error){
            console.log(error);
        }
    };

    
   
    useEffect(() => {
        listMedicoEspecialidades();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Especialidades Medicas</h1>
           <div className="row">
           {medicoespecialidade.map((medicoespecialidade) => (
               <MedicoEspecialidadesItem key ={medicoespecialidade.id} medicoespecialidade ={medicoespecialidade}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/medicoespecialidadesForm" role="button">Agregar Medico-Especialidades</Link>
               </div>
       </div>
   );
};

export default MedicoEspecialidadesList;