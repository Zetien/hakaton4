import { useEffect, useState } from 'react';
import { registerMedico } from '../Medicos/MedicosServer';
import { useNavigate, useParams } from "react-router-dom";
import * as MedicosServer from '../Medicos/MedicosServer';


const MedicosForm =()=>{
    const navigate = useNavigate();
    const params = useParams();

    

    const initialState = {
    nombres:'',
    apellidos:'',
    dni:'',
    direccion:'',
    correo:'',
    telefono:'',
    sexo:'',
    num_colegiatura:'',
    fecha_nacimiento:'',
    usuario_registro:'',
    usuario_modificacion:'',
    activo:'true'
};
    
    const [medico, setMedico] = useState(initialState);

    const handleInputChange = (e) => {
        setMedico({
            ...medico,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try{
            if(!params.id){
                registerMedico(medico).then((rest) => {console.log(rest);}).catch((err) => {console.log(err);});
            }else{
               await MedicosServer.updateMedico(params.id,medico); 
            }
            
            navigate("/medicosList");
        }catch(error){
            console.log(error);
        }
    };

    const getMedico=async(medicoId) => {
        try{
          const rest=await MedicosServer.getMedico(medicoId);
          const data=await rest.json();
          const {nombres,apellidos,dni,direccion,correo,telefono,sexo,num_colegiatura,fecha_nacimiento,usuario_registro,usuario_modificacion,activo}=data;
          setMedico({nombres,apellidos,dni,direccion,correo,telefono,sexo,num_colegiatura,fecha_nacimiento,usuario_registro,usuario_modificacion,activo});
          
        }catch(error){
            console.log(error);
        }
  
      };  

    useEffect(() => {
        if(params.id){
            getMedico(params.id);
        }
    },[]);

    return(
        <div>
            <h1 className="text-center mb-3 mt-3">Formulario Agregar Medico</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="nombreMedico" className="form-label">Nombres</label>
                    <input type="text" className="form-control" name="nombres" value={medico.nombres} onChange={handleInputChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="apellidoMedico" className="form-label">Apellidos</label>
                    <input type="text" className="form-control" name="apellidos" value={medico.apellidos} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="dniMedico" className="form-label">DNI</label>
                    <input type="number" className="form-control" name="dni" value={medico.dni} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="direccionMedico" className="form-label">Direccion</label>
                    <input type="text" className="form-control" name="direccion" value={medico.direccion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="correoMedico" className="form-label">Correo</label>
                    <input type="email" className="form-control" name="correo" value={medico.correo} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="telefonoMedico" className="form-label">Telefono</label>
                    <input type="number" className="form-control" name="telefono" value={medico.telefono} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="sexoMedico" className="form-label">Sexo</label>
                    <input type="text" className="form-control" name="sexo" value={medico.sexo} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="numColegiaturaMedico" className="form-label">Numero Colegiatura</label>
                    <input type="number" className="form-control" name="num_colegiatura" value={medico.num_colegiatura} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="fechaNacimientoMedico" className="form-label">Fecha Nacimiento</label>
                    <input type="date" className="form-control" name="fecha_nacimiento" value={medico.fecha_nacimiento} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioRegistro" className="form-label">Usuario Registro</label>
                    <input type="text" className="form-control" name="usuario_registro" value={medico.usuario_registro} onChange={handleInputChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="UsuarioModificacion" className="form-label">Usuario Modificacion</label>
                    <input type="text" className="form-control" name="usuario_modificacion" value={medico.usuario_modificacion} onChange={handleInputChange}/>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className="form-check-input" name="activo" defaultChecked={medico.activo}/>
                    <label className="form-check-label" htmlFor="exampleCheck1">Activo</label>
                </div>
                <div>
                    {params.id ? <button className="btn btn-primary">Actualizar</button> : <button className="btn btn-primary">Agregar</button>}
                    
                </div>
                
            </form>
        </div>
    )
};
export default MedicosForm;