from django.urls import path, include
from rest_framework import routers
from zetienclinic.views import *

router = routers.DefaultRouter()
router.register(r'medico', MedicoViewset)
router.register(r'paciente', PacienteViewset)
router.register(r'especialidade', EspecialidadeViewset)
router.register(r'medico_especialidade', Medico_especialidadeViewset)
router.register(r'horario', HorarioViewset)
router.register(r'cita', CitaViewset)

urlpatterns = [
    path('', include(router.urls)), 
]