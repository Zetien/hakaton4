import React from "react"
import { deleteHorario } from '../Horarios/HorariosServer';
import { useNavigate } from "react-router-dom";
import * as MedicosServer from '../Medicos/MedicosServer'
import { useEffect, useState } from "react";

const HorarioItem =({horario})=>{
    const navigate = useNavigate();
    const [medicos, setMedicos] = useState([]);

    const handleDelete=(horarioId)=>{
        deleteHorario(horarioId)
    
    };

    const buscarMedico=(medicoId)=>{
        return medicos.find((medico)=>
            medico.id===medicoId
        )?.nombres + " " + medicos.find((medico)=>
            medico.id===medicoId)?.apellidos;
    }

    

    const listMedicos = async() => {
        try{
          const res= await MedicosServer.listMedicos(); 
          const data = await res.json();
          setMedicos(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listMedicos();
    },[]);

    
    return(
      <div className="col-md-4">
            <div className="card border-primary card text-white bg-dark mb-3" style={{MaxWidth: '18rem'}}>
                <div className="card-header">{buscarMedico(horario.medico_id)}</div>
                <div className="card-body">
                    <h5 className="card-title">{horario.fecha_atencion}</h5>
                    <p className="card-text">Incio Atencion:&nbsp;{horario.inicio_atencion}</p>
                    <p className="card-text">Fin Atencion:&nbsp;{horario.fin_atencion}</p>
                    <div className="row">
                        <div className="col-3">
                            <form>
                                <button type="submit" onClick={()=>horario.id && handleDelete(horario.id)} className="btn btn-danger">Borrar</button>
                            </form>
                         </div>
                         <div className="col-3">
                            <button onClick={()=>navigate(`/updateHorario/${horario.id}`)} className="btn btn-success">Editar</button>
                         </div>
                    </div>
                    
                </div>
            </div>
      </div>
    )
};

export default HorarioItem;