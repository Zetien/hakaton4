import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/paciente/';

export const listPacientes = async () => {
    return await fetch(API_URL);
};

export const getPaciente=async(pacienteId) => {
    return await fetch(`${API_URL}${pacienteId}`);
}

export const registerPaciente = async (Paciente) => {
    console.log(Paciente);
    return axios.post(API_URL, Paciente,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updatePaciente = async (pacienteId,updatedPaciente) => {
    
    return axios.put(`${API_URL}${pacienteId}/`,updatedPaciente,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deletePaciente = async (pacienteId) => {
    return axios.delete(`${API_URL}${pacienteId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}

