import {Link} from 'react-router-dom'
const Footer = () => {
    return (
      
      <footer id="footer">
      <div className="footer-newsletter">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6">
              <h4>Desarrollando code por la salud</h4>
              <p>HAKATON ES SALUD</p>
              
            </div>
          </div>
        </div>
      </div>
      <div className="footer-top">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-6 footer-contact">
              <h3>Zetien Clinic<span>.</span></h3>
              <p>
                Barrio 13 de junio <br />
                Calle 31B #65-46<br />
                Cartagena de indias <br /><br />
                <strong>Telefono:</strong> 6516694<br />
                <strong>Correo:</strong> jzetien@lsv-tech.com<br />
              </p>
            </div>
            <div className="col-lg-3 col-md-6 footer-links">
              <h4>Nuestros Servicios</h4>
              <ul>
                <li><i className="bx bx-chevron-right" /> <Link to="/">Home</Link></li>
                <li><i className="bx bx-chevron-right" /> <Link to="/especialidadesList">Listar Especialidades</Link></li>
                <li><i className="bx bx-chevron-right" /> <Link to="/medicosList">Listar Medicos</Link></li>
                <li><i className="bx bx-chevron-right" /> <Link to="/pacientesList">Listar Pacientes</Link></li>
                <li><i className="bx bx-chevron-right" /> <Link to="/HorariosList">Listar Horarios</Link></li>
                <li><i className="bx bx-chevron-right" /> <Link to="/medicoEspecialidadesList">Listar Especialidades Medicas</Link></li>
              </ul>
            </div>
            <div className="col-lg-3 col-md-6 footer-links">
              <h4>Otros Links de interes</h4>
              <ul>
                <li><i className="bx bx-chevron-right" /> <a href="https://www.google.com/" target='_blank'>Google</a></li>
                <li><i className="bx bx-chevron-right" /> <a href="https://www.3djuegos.com/" target='_blank'>3DJuegos</a></li>
                <li><i className="bx bx-chevron-right" /> <a href="https://www.noticias3d.com/" target='_blank'>Noticias 3D</a></li>
                <li><i className="bx bx-chevron-right" /> <a href="https://ver3.cuevana3.cc/" target='_blank'>Cuevana 3</a></li>
                <li><i className="bx bx-chevron-right" /> <a href="https://compucalitv.org/" target='_blank'>CompucaliTV</a></li>
              </ul>
            </div>
            <div className="col-lg-3 col-md-6 footer-links">
              <h4>Redes Sociales</h4>
              <p>Dale like a nuestras publicaciones</p>
              <div className="social-links mt-3">
                <a href="https://es-la.facebook.com/zetien" className="facebook" target='_blank'><i className="bx bxl-facebook" /></a>
                <a href="https://www.instagram.com/jorgezetien/?hl=es" className="instagram" target='_blank'><i className="bx bxl-instagram" /></a>
                <a href="https://co.linkedin.com/in/jorge-luis-zetien-luna-674953149" className="linkedin" target='_blank'><i className="bx bxl-linkedin" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container py-4">
        <div className="copyright">
          © Copyright <strong><span>Jorge Zetien</span></strong>. All Rights Reserved
        </div>
        <div className="credits">
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </footer>
    )
}
export default Footer



