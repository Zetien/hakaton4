# HAKATON 4

## **Integrante**
- Jorge Luis Zetien Luna



> ### **Instrucciones para ejecutar el proyecto**
solo debes clonar el proyecto y descargarlo tendras una carpeta para el front y otra para el backend que se llama haka4
el front esta realizado con react y el back con django y esta dockerizado


> ### **Clonar proyecto**
-`git clone https://gitlab.com/Zetien/hakaton4.git`
-`cd frontreact`
-`cd haka4`



> ### **ejecutar el backend (directorio raiz haka4)**
-`docker-compose build`
-`docker-compose up`

> ### **configurar el admin para el backend (otra terminal en la raiz de haka4)**
-`docker-compose -f docker-compose.yml run --rm app python manage.py makemigrations`
-`docker-compose -f docker-compose.yml run --rm app python manage.py migrate`
-`docker-compose -f docker-compose.yml run --rm app python manage.py createsuperuser`

> ### **ejecutar el front (directorio raiz frontend)**
-`npm run dev`

instalar dependencias que estan en lista de abajo en caso de algna falla



> ### **Dependecias del front**
|       Key         |       value           |
|-------------------|-----------------------|
|   axios           |       0.27.2          |
|   bootsatrap      |       5.1.3           |
|   react           |       18.0.0          |
|   react-dom       |       18.0.0          |   
|  react-router-dom |       6.3.0           |

