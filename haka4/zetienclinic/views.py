from django.shortcuts import render
from zetienclinic.serializers import *
from rest_framework import permissions
from rest_framework import viewsets

class MedicoViewset(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer


class PacienteViewset(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer
    

class EspecialidadeViewset(viewsets.ModelViewSet):
    queryset = Especialidade.objects.all()
    serializer_class = EspecialidadeSerializer

class Medico_especialidadeViewset(viewsets.ModelViewSet):
    queryset = Medico_especialidade.objects.all()
    serializer_class = Medico_especialidadeSerializer
    

class HorarioViewset(viewsets.ModelViewSet):
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
    

class CitaViewset(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer
    
