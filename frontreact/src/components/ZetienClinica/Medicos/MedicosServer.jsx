import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/medico/';

export const listMedicos = async () => {
    return await fetch(API_URL);
};

export const getMedico=async(medicoId) => {
    return await fetch(`${API_URL}${medicoId}`);
}

export const registerMedico = async (Medico) => {
    console.log(Medico);
    return axios.post(API_URL, Medico,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updateMedico = async (medicoId,updatedMedico) => {
    
    return axios.put(`${API_URL}${medicoId}/`,updatedMedico,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deleteMedico = async (medicoId) => {
    return axios.delete(`${API_URL}${medicoId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}


