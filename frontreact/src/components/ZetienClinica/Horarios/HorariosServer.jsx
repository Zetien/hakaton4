import axios from 'axios';

const API_URL = 'http://127.0.0.1:8000/horario/';

export const listHorarios = async () => {
    return await fetch(API_URL);
};

 export const getHorario=async(horarioId) => {
    return await fetch(`${API_URL}${horarioId}`);
}; 




export const registerHorario = async (Horario) => {
    
    return axios.post(API_URL, Horario,{
        headers: {
            'Content-Type': 'application/json'
        },
    });
};

export const updateHorario = async (horarioId,updatedHorario) => {
    
    return axios.put(`${API_URL}${horarioId}/`,updatedHorario,{
        headers: {
            'Content-Type': 'application/json'
        },
       
    });
};

export const deleteHorario = async (horarioId) => {
    return axios.delete(`${API_URL}${horarioId}`,{
        headers: {
            'Content-Type': 'application/json'
        }
    });
}
