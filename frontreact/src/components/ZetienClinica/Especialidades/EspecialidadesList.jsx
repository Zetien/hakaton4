import React, {useEffect, useState} from 'react';
import * as EspecialidadesServer from '../Especialidades/EspecialidadesServer'
import EspecialidadItem from '../Especialidades/EspecialidadItem';
import {Link} from 'react-router-dom'

const EspecialidadesList = () => {
    const [especialidades, setEspecialidades] = useState([]);

    const listEspecialidades = async() => {
        try{
          const res= await EspecialidadesServer.listEspecialidades(); 
          const data = await res.json();
          setEspecialidades(data.results);
        }catch(error){
            console.log(error);
        }
    };
   
    useEffect(() => {
        listEspecialidades();
    },[]);
    
    
    return(
       <div className='container'>
        <h1 className="text-center mb-3 mt-3">Lista De Especialidades</h1>
           <div className="row">
           {especialidades.map((especialidad) => (
               <EspecialidadItem key ={especialidad.id} especialidad ={especialidad}/>
               ))}
            </div>
               <div className="mt-4 text-center">
               <Link name="" id="" className="btn btn-primary" to="/especialidadesForm" role="button">Agregar Especialidad</Link>
               </div>
       </div>
   );
};

export default EspecialidadesList;