import {Link} from 'react-router-dom'
const Navbar=()=>{
    return(
      <div>
      <section id="topbar" className="d-flex align-items-center">
        <div className="container d-flex justify-content-center justify-content-md-between">
          <div className="contact-info d-flex align-items-center">
            <i className="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@example.com">jzetien@lsv-tech.com</a></i>
            <i className="bi bi-phone d-flex align-items-center ms-4"><span>3145022064</span></i>
          </div>
          <div className="social-links d-none d-md-flex align-items-center">
            <a href="https://es-la.facebook.com/zetien" className="facebook" target='_blank'><i className="bi bi-facebook" /></a>
            <a href="https://www.instagram.com/jorgezetien/?hl=es" className="instagram" target='_blank'><i className="bi bi-instagram" /></a>
            <a href="https://co.linkedin.com/in/jorge-luis-zetien-luna-674953149" className="linkedin" target='_blank'><i className="bi bi-linkedin" /></a>
          </div>
        </div>
      </section>
      <header id="header" className="d-flex align-items-center">
        <div className="container d-flex align-items-center justify-content-between">
          <h1 className="logo"><Link to="/">Zetien Clinic<span>.</span></Link></h1>
          <nav id="navbar" className="navbar">
            <ul>
              <li><Link className="nav-link scrollto active" to="/">Home</Link></li>
              <li><Link className="nav-link scrollto" to="/especialidadesList">Especialidades</Link></li>
              <li><Link className="nav-link scrollto" to="/medicosList">Medicos</Link></li>
              <li><Link className="nav-link scrollto " to="/pacientesList">Pacientes</Link></li>
              <li><Link className="nav-link scrollto" to="/HorariosList">Horarios</Link></li>
              <li><Link className="nav-link scrollto" to="/medicoEspecialidadesList">Medicos - Especialidades</Link></li>
              <li><Link className="nav-link scrollto" to="/citasList">Citas</Link></li>          
            </ul>
            <i className="bi bi-list mobile-nav-toggle" />
          </nav>
        </div>
      </header>
    </div>
    )
}
export default Navbar;



